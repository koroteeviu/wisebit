<?php

namespace app\commands;

use app\models\Video;
use Yii;
use yii\console\ExitCode;
use yii\db\Exception;

/**
 *
 * $totalCount = 10000000, $batchSize = 5000
 * Class FakeDataController
 * @package app\commands
 */
class FakeDataController extends \yii\console\Controller
{

	public $totalCount = self::DEFAULT_TOTAL;
	public $batchSize = self::DEFAULT_BATCH;

	const DEFAULT_TOTAL = 1000000;

	const DEFAULT_BATCH = 5000;

	public function getActionArgsHelp($action)
	{

	}

	public function options($actionID)
	{
		return ['total', 'batch'];
	}

	public function optionAliases()
	{
		return ['t' => 'total', 'b' => 'batch'];
	}


	/**
	 * test info
	 * @return int
	 */
	public function actionIndex()
	{
		$videoModel = new Video();
		$batch = 0;
		$rows = [];
		$faker = \Faker\Factory::create();

		$connection =Yii::$app->db;

		//Speedup import
		$connection->enableLogging = false;
		$connection->enableProfiling = false;

		for ($i = 1; $i <= $this->totalCount; $i++) {
			$rows[] = [
				'title' => $faker->sentence($nbWords = 4, $variableNbWords = true),
				'thumbnail' => 'http://lorempixel.com/200/200/cats/',
				'duration' => $faker->numberBetween(60, 60 * 90),
				'views' => $faker->randomNumber(),
				'added_datetime' => $faker->dateTimeBetween($startDate = '-2 years', $endDate = 'now')->format('Y-m-d H:i:s'),
			];
			$batch++;
			if ($this->batchSize === $batch) {
				try {
					$connection->createCommand()->batchInsert(Video::tableName(),
						['title', 'thumbnail', 'duration', 'views', 'added_datetime'], $rows)->execute();
				} catch (Exception $e) {
					$this->stderr($e->getMessage());
					return ExitCode::CONFIG;
				}
				$this->stdout(sprintf("Imported %s", $i . PHP_EOL));
				$batch = 0;
				$rows = [];
			}
		}
		$connection->createCommand()->batchInsert(Video::tableName(), $videoModel->getAttributes(null, ['id']), $rows);
		return ExitCode::OK;
	}
}