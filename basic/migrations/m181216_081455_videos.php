<?php

use yii\db\Migration;

/**
 * Class m181216_081455_videos
 */
class m181216_081455_videos extends Migration
{

	public function up()
	{
		$this->execute('
		CREATE TABLE IF NOT EXISTS videos (
			id BIGSERIAL PRIMARY KEY,
			title TEXT NOT NULL,
			thumbnail TEXT NOT NULL,
			duration INTEGER NOT NULL,
			views BIGINT NOT NULL default 0,
			added_datetime TIMESTAMP NOT NULL default NOW()
		);');
		
		$this->execute('CREATE TRIGGER videos_count BEFORE INSERT OR DELETE ON videos
		  FOR EACH ROW EXECUTE PROCEDURE adjust_count();
		');

		$this->execute('
			INSERT INTO row_counts (relname, reltuples)
			  VALUES (\'videos\', (SELECT count(*) from videos));
		');
	}

	public function down()
	{
		$this->dropTable('videos');
		$this->execute('DROP TRIGGER videos_count;');
	}
}
