<?php

use yii\db\Migration;

/**
 * Class m181216_081454_row_count
 */
class m181216_081454_row_count extends Migration
{
	/**
	 * {@inheritdoc}
	 */
	public function safeUp()
	{
		$this->execute('
		CREATE TABLE row_counts (
		  relname text PRIMARY KEY,
		  reltuples bigint
		);');
		
		
		$this->execute('CREATE OR REPLACE FUNCTION adjust_count()
		RETURNS TRIGGER AS
		$$
			DECLARE
			BEGIN
			IF TG_OP = \'INSERT\' THEN
				EXECUTE \'UPDATE row_counts set reltuples=reltuples +1 where relname = \'\'\' || TG_RELNAME || \'\'\'\';
				RETURN NEW;
			ELSIF TG_OP = \'DELETE\' THEN
				EXECUTE \'UPDATE row_counts set reltuples=reltuples -1 where relname = \'\'\' || TG_RELNAME || \'\'\'\';
				RETURN OLD;
			END IF;
			END;
		$$
		LANGUAGE \'plpgsql\';
		');
	}

	/**
	 * {@inheritdoc}
	 */
	public function safeDown()
	{
		$this->dropTable('row_counts');
		$this->execute('DROP FUNCTION adjust_count;');

		return false;
	}

}
