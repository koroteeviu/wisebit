<?php
/* @var $this yii\web\View */

/* @var $model app\models\Video */

use yii\data\Sort;
use yii\widgets\LinkPager;

?>
<div>
	<?php
	echo LinkPager::widget([
		'pagination' => $pagination,
	]);
	?>
</div>

<?php
/**
 * @var Sort $sort
 */
echo $sort->link('views') . ' | ' . $sort->link('added_datetime');
?>
<div class="album py-5 bg-light">
    <div class="container">
        <div class="row">
			<?php
            $models = $provider->getModels();
			foreach ($models as $model) {
				echo $this->render('list_items', ['model' => $model]);
			}
			?>
        </div>
    </div>
</div>