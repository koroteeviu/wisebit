<?php

namespace app\models\video;

use app\models\pagination\SortInterface;

class VideoSort implements SortInterface
{

	protected $sort = [];
	protected $attributes = [
		'added_datetime',
		'views'];

	public function byViews($sort)
	{
		//sort by one field only
		$this->sort = [];

		$this->sort['views'] = $sort;
		return $this;
	}

	public function byDate($sort)
	{
		$this->sort = [];
		$this->sort['added_datetime'] = $sort;
		return $this;
	}

	public function getSort(): array
	{
		return $this->sort;
	}

	/**
	 * @return array
	 */
	public function getAttributes(): array
	{
		return $this->attributes;
	}
}