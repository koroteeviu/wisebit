<?php

namespace app\models\video;


use app\models\pagination\PagedPagination;
use app\models\pagination\PaginatorInterface;
use app\models\pagination\SortInterface;
use app\models\pagination\StorageInterface;
use app\models\RowCount;
use app\models\Video;
use yii\data\BaseDataProvider;
use yii\data\Pagination;
use yii\data\Sort;

class   VideoStorage extends BaseDataProvider implements StorageInterface
{

	/**
	 * @var VideoSort
	 */
	protected $sorting;
	/**
	 * @var PagedPagination
	 */
	protected $pagination;

	protected $totalCount = 0;

	/**
	 * @param PagedPagination $paginator
	 * @return VideoStorage
	 */
	public function withPagination(PaginatorInterface $paginator)
	{
		$this->pagination = $paginator;
		$this->setPagination(new Pagination(
				[
					'defaultPageSize' => 20,
					'forcePageParam' => true,
					'pageSize' => $paginator->getCount(),
					'totalCount' => $this->getTotalCount()
				])
		);
		return $this;
	}

	/**
	 * @param VideoSort $sort
	 * @return VideoStorage
	 */
	public function withSorting(SortInterface $sort)
	{
		$this->sorting = $sort;
		$this->setSort(new Sort(
			[
				'attributes' => $sort->getAttributes()
			]
		));
		return $this;
	}

	/**
	 * Prepares the data models that will be made available in the current page.
	 * @return array the available data models
	 * @throws \yii\db\Exception
	 */
	protected function prepareModels()
	{
		if ($this->getSort()->getAttributeOrder('added_datetime')) {
			$orders = ['id' => $this->getSort()->getAttributeOrder('added_datetime')];
			return Video::find()->where([
				'>',
				'id',
				$this->pagination->getCount() * $this->pagination->getPageNumber()
			])->orderBy($orders)->limit($this->pagination->getCount())->all();
		} else {
			$connection = \Yii::$app->getDb();
			$query = sprintf("
				   WITH bookmark AS (
		             SELECT (histogram_bounds::text::int[])[(( %s * %s) / 100000)+1] AS start,
					           (histogram_bounds::text::int[])[((%s * %s) / 100000)+2] AS stop
					    FROM pg_stats
					    WHERE tablename = 'videos'
					    AND attname = 'views'
					    LIMIT 1
					  )
					SELECT *
					FROM videos
					WHERE views >= (select start from bookmark)
					AND views < (select stop from bookmark)
					ORDER BY views %s LIMIT %s
					OFFSET ((%s *%s) %% 100000);",
				$this->pagination->getPageNumber(), $this->pagination->getCount(),
				$this->pagination->getPageNumber(), $this->pagination->getCount(),
				$this->sort->getAttributeOrder('views') == SORT_ASC ? 'ASC' : 'DESC', $this->pagination->getCount(),
				$this->pagination->getPageNumber(), $this->pagination->getCount());
			$command = $connection->createCommand($query);
			$queryResult = $command->queryAll();

			$result = [];
			foreach ($queryResult as $res) {
				$video = new Video();
				$video->load(['1' => $res], 1);
				$result[] = $video;
			}
			return $result;

		}

	}

	/**
	 * Prepares the keys associated with the currently available data models.
	 * @param array $models the available data models
	 * @return array the keys
	 */
	protected function prepareKeys($models)
	{
		$result = [];
		/**
		 * @var Video $model
		 */
		foreach ($models as $model) {
			$result[] = $model->id;
		}
		return $result;
	}

	/**
	 * Returns a value indicating the total number of data models in this data provider.
	 * @return int total number of data models in this data provider.
	 */
	protected function prepareTotalCount()
	{
		if ($this->totalCount != 0) {
			return $this->totalCount;
		}
		/**
		 * @var RowCount $data
		 */
		$data = RowCount::find()->where(['relname' => Video::tableName()])->one();
		if ($data === null) {
			$this->totalCount = Video::find()->count();
		} else {
			$this->totalCount = $data->reltuples;
		}
		return $this->totalCount;
	}
}