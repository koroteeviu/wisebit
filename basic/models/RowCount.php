<?php

namespace app\models;


/**
 * This is the model class for table "row_counts".
 *
 * @property string $relname
 * @property int $reltuples
 */
class RowCount extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'row_counts';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['relname'], 'required'],
            [['relname'], 'string'],
            [['reltuples'], 'default', 'value' => null],
            [['reltuples'], 'integer'],
            [['relname'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'relname' => 'Relname',
            'reltuples' => 'Reltuples',
        ];
    }
}
