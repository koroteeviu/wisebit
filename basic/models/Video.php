<?php

namespace app\models;


/**
 * This is the model class for table "videos".
 *
 * @property int $id
 * @property string $title
 * @property string $thumbnail
 * @property int $duration
 * @property int $views
 * @property string $added_datetime
 */
class Video extends \yii\db\ActiveRecord
{
	/**
	 * {@inheritdoc}
	 */
	public static function tableName()
	{
		return 'videos';
	}

	/**
	 * {@inheritdoc}
	 */
	public function rules()
	{
		return [
			[['title', 'thumbnail'], 'string'],
			[['duration', 'views'], 'default', 'value' => null],
			[['duration', 'views'], 'integer'],
			[['added_datetime'], 'safe'],
		];
	}

	/**
	 * {@inheritdoc}
	 */
	public function attributeLabels()
	{
		return [
			'id' => 'ID',
			'title' => 'Title',
			'thumbnail' => 'Thumbnail',
			'duration' => 'Duration',
			'views' => 'Views',
			'added_datetime' => 'Added Datetime',
		];
	}
}
