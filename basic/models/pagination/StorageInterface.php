<?php

namespace app\models\pagination;

interface StorageInterface
{

	public function withPagination(PaginatorInterface $paginator);

	public function withSorting(SortInterface $sort);
}