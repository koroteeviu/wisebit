<?php

namespace app\models\pagination;

use InvalidArgumentException;

class PagedPagination implements PaginatorInterface
{
	private $pageNumber;

	private $count;

	public function __construct(int $pageNumber, int $count)
	{
		if ($pageNumber < 1) {
			throw new InvalidArgumentException('negative page number');
		}
		if ($count < 0) {
			throw new InvalidArgumentException('negative count per page');
		}
		$this->pageNumber = $pageNumber;
		$this->count = $count;
	}

	/**
	 * @return int
	 */
	public function getPageNumber(): int
	{
		return $this->pageNumber;
	}

	/**
	 * @return int
	 */
	public function getCount(): int
	{
		return $this->count;
	}
}