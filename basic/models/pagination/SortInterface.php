<?php

namespace app\models\pagination;

interface SortInterface
{
	const SORT_ASC = 'ASC';
	const SORT_DESC = 'DESC';

	public function getSort(): array;
}