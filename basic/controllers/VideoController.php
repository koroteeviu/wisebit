<?php

namespace app\controllers;

use app\models\pagination\PagedPagination;
use app\models\Video;
use app\models\video\VideoSort;
use app\models\video\VideoSortSpecification;
use app\models\video\VideoStorage;
use yii\data\Sort;
use yii\web\Controller;


class VideoController extends Controller
{


	public function actionVideo()
	{
		$id = \Yii::$app->request->get('id');
		$videos = Video::find()->where(['id' => $id])->one();
		return $this->render('index');
	}

	/**
	 * @return string
	 */
	public function actionVideos()
	{
		$page = \Yii::$app->request->get('page');
		$sort = new Sort([
				'attributes' => [
					'views',
					'added_datetime'
				],
			]
		);
		$provider = new VideoStorage(
			[
				'sort' => $sort
			]
		);
		$provider->withPagination(new PagedPagination($page, 20));


		return $this->render('index', [
			'provider' => $provider,
			'pagination' => $provider->getPagination(),
			'sort' => $sort,
		]);
	}

}