<?php

namespace yii\helpers;

class DateHelper
{

	public static function durationForView($duration): string
	{
		if ($duration > 3600) {

			return gmdate("H:i:s", $duration);
		} elseif ($duration > 0) {
			return gmdate("i:s", $duration);
		} else {
			return "";
		}

	}
}