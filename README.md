### Run project

Start docker compose:

`docker-compose up -d`

Run bash in php container:

```
PHP_CONTAINER=$(docker ps | grep php | awk '{print $NF}') &&
DB_CONTAINERR=$(docker ps | grep db | awk '{print $NF}') &&
docker exec -it $PHP_CONTAINER php /var/www/html/composer.phar install &&
docker exec -it $PHP_CONTAINER php yii migrate/up --interactive=0 &&
docker exec -it $PHP_CONTAINER php yii fake-data &&
docker exec -it $DB_CONTAINER psql -U wisebit -c 'ALTER TABLE videos ALTER COLUMN views SET statistics 10000;' &&
docker exec -it $DB_CONTAINER psql -U wisebit -c 'VACUUM ANALYZE;' 
```
Add wisebit host to /etc/hosts

Run:
```
 wrk -t10 -c50 -d10s --timeout 1 -R 200 -s wrk.lua http://wisebit/videos/
``` 