function getRandomSort()
    selection = math.random(1, 4)
    if selection == 1 then return "-view" end
    if selection == 2 then return "view" end
    if selection == 3 then return "-added_datetime" end
    if selection == 4 then return "added_datetime" end
    return ""
end

function getRandomPage()
	return math.random(1,60000)
end



-- add a random string to the original request path.
request = function()
    local path = wrk.path ..getRandomPage() .."?sort=" ..getRandomSort()
    return wrk.format(wrk.method, path, wrk.headers, wrk.body)
end
